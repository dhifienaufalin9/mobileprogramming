import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, ImageBackground, Image, SafeAreaView, TextInput, TouchableOpacity } from 'react-native';

export default function Login() {
  const [text,onChangeText]=React.useState()
  
  const [text1,onChangeText1]=React.useState()
  return (

<View style={styles.container}>
  <ImageBackground style={styles.bg} source={require('./../assets/Img/bis.png')}>
  <StatusBar style="auto" />
  <View style={styles.silu}/>
 
    <View style= {styles.mid}>
      <Image source={require('./../assets/Img/stradi.png')} style= {styles.logo}/>
    </View>
    <View style= {styles.mid}>
      <Text style={styles.title}>STRADIMOUR</Text>
    </View>
     <View style= {styles.mid}>
      <Text style={styles.io}>Log In</Text>
    </View>
    <View style= {styles.mid}>
      <Text style={styles.des}>Forgot password? Or Log In with</Text>
    </View>
      
    <SafeAreaView>
      <View style= {styles.mid}>
        <TextInput
            style={styles.tname}
            placeholder='    Email or Username'
            placeholderTextColor= '#356C7D'
            onChangeText={onChangeText} value={text}/>
      </View>
    </SafeAreaView>
    <View style= {styles.mid}>
      <View style= {styles.mid}>
        <SafeAreaView>
          <TextInput
            style={styles.tpass}
            placeholder='    Password'
            placeholderTextColor= '#356C7D'
            onChangeText1={onChangeText1} value={text1}/>
        </SafeAreaView>
      </View>
     
      <TouchableOpacity
      
          style={styles.btlog}     
          onPress={() => window.location.href = 'Login.js'}>
        <View style= {styles.mid}>
          <Text style= {styles.log}>Log In</Text>
        </View>
        
      </TouchableOpacity>
      
    </View>
  </ImageBackground>
</View>
);
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

bg: {
    width:360, 
    height:640
  },

input: {
    flex:1,
    position: 'absolute',
    color: 'white'
  },

back: {
    top : -635,
    right: 160,
    alignItems:'center',
    justifyContent: 'center',
    height: 45,
    width: 45
  },

logo: {
    top : -695,
    left: 8,
    alignItems:'center',
    justifyContent: 'center',
    height: 170,
    width: 170
  },

silu:{
  margin:20,
    backgroundColor:'#DFE9E3',
    width: '100%',
    height:'100%',
    opacity: 0,
  },

  title:{
    position: 'absolute',
    top: -735,
    fontSize: 14,
    color: '#DFE9E3'
  },
  io:{
    left: 50,
    position: 'absolute',
    top: -640,
    fontSize: 30,
    color: '#DFE9E3'
  },
  des: {
    left: 80,
    textAlign: 'center',
    position: 'absolute',
    top: -320,
    fontSize: 14,
    color: '#DFE9E3'
  },

  mid:{
    alignItems:'center'
  },

  btlog: {
    position:'absolute',
    borderRadius:72,
    borderColor: 'white',
    borderWidth: 1,
    alignItems:'center',
    bottom: 420,
    backgroundColor: '#DFE9E3',
    width:264,
    height:46
  },

  log:{
    textAlignVertical: 'center',
    fontSize: 35,
    color: '#356C7D',
     
  },

  tname: {
    position:'absolute',
    borderRadius:72,
    borderColor: 'white',
    borderWidth: 1,
    alignItems:'center',
    bottom : 530,
    backgroundColor: "#DFE9E3",
    width:264,
    height:46
  },

  tpass: {
    position: 'relative',
    borderRadius:72,
    borderColor: 'white',
    borderWidth: 1,
    alignItems:'center',
    bottom : 510,
    backgroundColor: "#DFE9E3",
    width:264,
    height:46
  },

  tex:{
    top: 9,
    left: -52,
    fontSize: 13,
    color: 'white'
  },

  tex1:{
    position: 'relative',
    top: 9,
    left: -99,
    fontSize: 13,
    color: 'white'
  }
});
