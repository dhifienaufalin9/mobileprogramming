import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, ImageBackground ,Image, TouchableOpacity} from 'react-native';



export default function App() {
  
  return (
      
    <View style={styles.container}>
        <ImageBackground style={styles.bg} source={require('./../assets/Img/wp.png')}>
      <StatusBar style="auto" />
      <View style= {styles.mid}>

      <Image source={require('./../assets/Img/bb.png')} style= {styles.back}/>

      <Image source={require('./../assets/Img/ava.jpg')} style= {styles.logo}/>
      </View>
<View style={styles.nmdepan}>
      <Text style={styles.nm}>Dhifie </Text></View>
     

      <View style={styles.nmbelakang}>
      <Text style={styles.nm}>Naufalindhia </Text>
      </View>
<View style={styles.tex1}>
      <Text style={styles.tex}>Hi I’m Dhifie Naufalindhia I’m student at</Text>
      <Text style={styles.tex}>STT Wastukancana </Text>
      <Text style={styles.tex}>with a Major in Informatic Engineering</Text>
      <Text style={styles.tex}> </Text>
      <Text style={styles.tex}> </Text>

      <Text style={styles.tex}>My Class is Morning A</Text>
      
</View>

<View style={ styles.contact}>
    <Text style={styles.contactfont}>Contact Info</Text>
    </View>

<View style= {styles.mp}>   
    <Text style={styles.dtfont}>087764591550</Text>
    </View>

<View style= {styles.no}>
    <Text style={styles.nop}>Jl. Veteran Gg Soka 2 Rt 46/05</Text>
    </View>

<View style= {styles.igg}>
    <Text style={styles.iig}>@dhifienaufalin</Text>
    </View>

<View style= {styles.mai}>
    <Text style={styles.ma}>dhifie99@gmail.com</Text>
    </View>

<View style= {styles.tex1}>
      <Image source={require('./../assets/Img/map.png')} style= {styles.map}/>
      </View>
    
<View style= {styles.tex1}>
      <Image source={require('./../assets/Img/phone.png')} style= {styles.phone}/>
      </View>
         
<View style= {styles.tex1}>
      <Image source={require('./../assets/Img/ig.png')} style= {styles.ig}/>
      </View>
         
<View style= {styles.tex1}>
      <Image source={require('./../assets/Img/mail.png')} style= {styles.mail}/>
      </View>
           
<View style= {styles.tex1}>
      <Image source={require('./../assets/Img/lin.png')} style= {styles.lin}/>
      </View>
      </ImageBackground>
      </View>
            
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  bg: {
    width:360, 
    height:640
  },
  logo: {
    position:'relative',
    top : 45,
    right: 100,
    alignItems:'center',
    justifyContent: 'center',
    height: 136,
    width: 113
  },
  dtfont:{
    top: 235,
    position:'absolute',
    textAlign: 'center',
    fontSize: 18,
    color: '#2C3357'
  },
  nop:{
    position:'absolute',
    textAlign: 'center',
    fontSize: 18,
    color: '#2C3357'
  },
  iig:{
    position:'absolute',
    textAlign: 'center',
    fontSize: 18,
    color: '#2C3357'
  },
  igg:{
    top:135,
    right:-80,
  },
  ma:{
    position:'absolute',
    textAlign: 'center',
    fontSize: 18,
    color: '#2C3357'
  },
  mai:{
    top:195,
    right:-80,
  },
  no:{
    top:73,
    right:-80,
  },
  mp:{
    top:20,
    right:-80,
  },
  map: {
    position:'relative',
    top : 107,
    right: 140,
    alignItems:'center',
    justifyContent: 'center',
    height: 40,
    width: 40
  },
  lin: {
    position:'relative',
    top :-135,
    right: 116,
    alignItems:'center',
    justifyContent: 'center',
    height: 136,
    width: 113
  },
  phone: {
    position:'relative',
    top: 255,
    right: 140,
    alignItems:'center',
    justifyContent: 'center',
    height: 35,
    width: 35
  },
  ig: {
    position:'relative',
    top : 95,
    right: 140,
    alignItems:'center',
    justifyContent: 'center',
    height: 35,
    width: 40
  },
  mail: {
    position:'relative',
    top : 123,
    right: 140,
    alignItems:'center',
    justifyContent: 'center',
    height: 35,
    width: 40
  },
  nm:{
    top: 10,
    fontSize: 36,
    color: '#2C3357'
  },

  tex:{
    top: 30,
    textAlign: 'center',
    fontSize: 14,
    color: '#2C3357'
  },
  contactfont:{
    top: 55,
    textAlign: 'center',
    fontSize: 18,
    color: '#2C3357'
  },
  contact:{
    position: 'absolute',
    top:267,
    left:-110,
    right:120,

  },
  tex1:{
    alignItems: 'center',
    top:-40,
    right:0,

  },
  nmdepan:{
    alignItems: 'center',
    top:-70,
    right: -20

  },
  nmbelakang:{
    top: -70,
    right: -150

  },
  desc: {
    top: -580,
    fontSize: 14,
    color: '#868686'

  },
  des: {
    top: -470,
    fontSize: 18,
    color: 'white',
    fontWeight: 'bold'

  },

  mid:{
    alignItems:'center'
  },
  btnlogin: {
    position:'absolute',
    borderRadius:8,
    alignItems:'center',
    top: -440,
    backgroundColor: "#DDDDDD23",
    width:198,
    height:36
    
  },
  login:{
    alignItems: 'center',
    fontSize: 21,
    color: 'white'
  },
  btnsignup: {
    position:'absolute',
    borderRadius:8,
    alignItems:'center',
    top: -390,
    backgroundColor: "#DDDDDD23",
    width:198,
    height:36
    
  },
  signup:{
    alignItems: 'center',
    fontSize: 21,
    color: 'white'
  },
  back: {
    position:'absolute',
    top:15,
    left:10,
    alignItems:'center',
    justifyContent: 'center',
    height: 20,
    width: 20
  },


});

