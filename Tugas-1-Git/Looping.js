//LOOPING WHILE
console.log("");
var angka =2; 
console.log(" LOOPING PERTAMA ");
while (angka<=20){
    console.log(angka+" - I LOVE CODING ");
    angka+=2;
}

console.log("");
var angka1 =20;
console.log(" LOOPING KEDUA ");
while (angka1 <=20 && angka1 >=2 ){
    console.log(angka1+ " - I WILL BECOME A MOBILE DEVELOPER ");
    angka1-=2;

}


//LOOPING FOR
console.log("");
for (var angka2=1; angka2<=20; angka2+=1){
    if(angka2 % 3== 0 && angka2 %2 !=0){
        console.log(angka2 + " - I Love Coding");
    }else if(angka2 % 2!=0){
        console.log(angka2 +" - Teknik");
    }else if(angka2 % 2==0){
        console.log(angka2 +" - Informatika")
    }
}


//PERSEGI PANJANG
console.log("");
var d="";
for (var i=1; i<=4; i++){
    for (var j=1; j<=8; j++){
        d += "#";

    }
    d += "\n";
}
console.log(d);


//TANGGA
console.log("");
var h="";
for (var i=1; i<=7; i++){
    for(var j=1; j<=i; j++){
        h += "#";
    }
    h += "\n";

}
console.log(h);


//PAPAN CATUR
console.log("");
var f ="";
for (var i=1; i<=8; i++){
    for (var j=1; j<=8; j++){
        if (i % 2 !=0 && j%2 != 0){
            f += " #";

        }else if (i % 2 ==0 && j%2 ==0){
            f += "# ";
        }  
    }f += "\n";
}
console.log(f);