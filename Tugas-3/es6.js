//No 1
console.log("\n  No 1 - Mengubah fungsi menjadi arrow ")
const golden = function goldenFunction(){
    console.log("this is golden!!")
  }
   
  golden()
  console.log("")

//No 2
console.log("\n No 2 - Sederhanakan menjadi Object literal di ES6 ")
const newFunction = (firstName, lastName)=> {
  return{
    firstName: firstName,
    lastName: lastName,
    fullname: () =>
    console.log(firstName + " " + lastName)
  }
}
newFunction("Wiliam", "Imoh").fullname()
console.log("")

//No 3
console.log("\n No 3 - Destructuring ")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation, spell } = newObject;
console.log(firstName, lastName, destination, occupation)
console.log("")

//No 4
console.log("\n No.4 - Array Spreading ")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east];

console.log(combined);
console.log("")


//No 5
console.log("\n No.5 - Template Literals ")
const planet = "earts"
const view = "glass"
var before = `Lorem` + view + `dolo sit amet,` + 
`consectetur adipiscing elit,` + planet + `do eiusmod tempor ` +
 `incididunt ut labore et dolore magna aliqua. Ut enim` +
 `ad minim veniam`
 //Driver Code
 console.log(before)
 console.log("")

