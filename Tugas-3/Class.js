//No 1
console.log("\n No. 1 - Animal Class ")
class Animal {
    //Code Class
    constructor(name){
        this.Animalname=name;
        this.AnimalLegs=4;
        this.Animal_blooded=false;
        console.log("")

    }
    get name(){
        return this.Animalname;  
    }
    get legs(){
        return this.AnimalLegs;
    }
    get cold_blooded(){
        return this.Animal_blooded;
    }

    set name(a){
        this.Animalname=a;
    }
    set legs(b){
        this.AnimalLegs=b;
    }
    set cold_blooded(d){
        this.Animal_blooded=c;
    }
}

//Code Class Ape dan Class Frog
class Ape extends Animal{
    constructor(name){
        super(name)
        this.Animalname=name;
        this.AnimalLegs=2;
    }yell(){
        return console.log('auoo')
    }
}
class frog extends Animal{
    constructor(name){
        super(name)
        this.Animalname=name;
    }jump(){
        return console.log('hop hop')
    }
}
let sheep = new Animal("Shaun");
console.log(sheep.name) // "Shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
console.log("")

let sungkong = new Ape("Kera Sakti")
console.log(sungkong.name) // "Shaun"
console.log(sungkong.legs) // 4
console.log(sungkong.cold_blooded) // false
sungkong.yell() // "auoo"
console.log("")

let kodok = new frog("buduk")
console.log(kodok.name) // "Shaun"
console.log(kodok.legs) // 4
console.log(kodok.cold_blooded) // false
kodok.jump() // "hop hop"
console.log("")

//No 2
console.log("\n No.2 - Function to Class ")
class Clock {
    constructor({template}){
        this.template=template;
    } 
    render(){
        let date = new Date();
        
        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        let output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(output);
    }
    stop(){
        clearInterval(timer);
    };
    start(){
        this.render();
        this.timer = setInterval(() => this.render(),1000);
    }

}
let clock= new Clock({template: 'h:m:s'});
clock.start();
console.log("")