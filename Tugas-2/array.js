//No 1
console.log("\n  No 1 (Range)")
function range(startNum, finishNum) {
    var array = []
    if (startNum < finishNum) {
        for (var j = startNum; j <= finishNum; j++) {
            array.push(j)
        }
    } else if (startNum > finishNum) {
        for (var j = startNum; j >= finishNum; j--) {
            array.push(j)
        }
    } else {
        array.push(-1)    
    }
    return array
}
console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())
console.log()

console.log("\n No 2 (Range with Step)")
function rangeWithStep(startNum, finishNum, step) {
    var array = []
    if (startNum < finishNum) {
        for (var j = startNum; j <= finishNum; j+=step) {
            array.push(j)
        }
    } else if (startNum > finishNum) {
        for (var j = startNum; j >= finishNum; j-=step) {
            array.push(j)
        }
    } else {
        array.push(-1)    
    }
    return array
}
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))
console.log()

console.log("\n No 3 (Sum Of Range)")
function sum(start, finish, step) {
    var array = [];
    if (start == null && finish == null && step == null) {
        array.push(0);
        var sum = array[0];
    }
    else if (start < finish && step == null) {
        for (var i = start; i <= finish; i++) {
            array.push(i);
        }
        var sum = array.reduce((a, b) => { return a + b; }, 0)
    }
    else if (start > finish && step == null) {
        for (var j = start; j >= finish; j--) {
            array.push(j);
        }
        var sum = array.reduce((a, b) => { return a + b; }, 0)
    }
    else if (start < finish) {
        for (var k = start; k <= finish; k += step) {
            array.push(k);
        }
        var sum = array.reduce((a, b) => { return a + b; }, 0)
    }
    else if (start > finish) {
        for (var l = start; l >= finish; l -= step) {
            array.push(l);
        }
        var sum = array.reduce((a, b) => { return a + b; }, 0)
    }
    else if (finish == null && step == null) {
        array.push(1);
        var sum = array.reduce((a, b) => { return a + b; }, 0)
    }


    return sum;
}
console.log(sum(1, 10))
console.log(sum(5, 50, 2))
console.log(sum(15, 10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum())
console.log("");

console.log("\n No 4 (Array Multidimensi)")
function dataHandling(numbers) {
     var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ]
     for (var x= numbers; x < 4; x++) {   
         console.log('Nomor ID: ' + input[x][0])
         console.log('Nama Lengkap: ' + input[x][1])
         console.log('TTL: ' + input[x][2] + ' ' + input[x][3])
         console.log('Hobi: ' + input[x][4])
         console.log()
        }return"";
}
console.log(dataHandling(0))
console.log()

console.log("\n No 5 (Balik Kata)")
function balikKata(str) {
    var currentString = str;
    var newString = '';
    for (var i = str.length - 1; i >= 0; i--) {
        newString = newString + currentString[i];
    }
    return newString;
}
console.log(balikKata("Kasur Rusak"))
console.log(balikKata("Informatika"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Humanikers"))
console.log()

console.log("\n No 6 (Metode Array)")

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]

function dataHandling2(biodata) {
    
    input.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
    input.splice(4, 1, "Pria" , "SMA Internasional Metro");
    console.log(input)
    
    var inputSplit = input[3].split("/")
    var inputJoin= inputSplit.join("-")
    var bulan = inputJoin[3] + inputJoin[4]
    
    switch(bulan) {
      case '01': console.log('Januari'); break;
      case '02': console.log(' Februari '); break; 
      case '03': console.log(' Maret '); break; 
      case '04': console.log(' April '); break; 
      case '05': console.log (' Mei '); break; 
      case '06': console.log(' Juni '); break; 
      case '07': console.log(' Juli '); break; 
      case '08': console.log(' Agustus '); break; 
      case '09': console.log(' September '); break; 
      case '10': console.log(' Oktober '); break; 
      case '11': console.log(' November '); break; 
      case '12': console.log(' Desember '); break; 
    }
    
    var inputShortDes = inputSplit.sort(function(a, b) {return a - b});
    console.log(inputShortDes)
    
    var tes = input[3].split("/")
    console.log(tes.join("-"))
    
    console.log(input[1].toString().slice(0,14))
    return biodata
    }
    dataHandling2();

